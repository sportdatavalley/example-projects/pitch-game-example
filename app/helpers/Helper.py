from _datetime import datetime

def build_input_config(one_location = 'null',preloaded = 'null',disable_button = 'null',task_log = 'null',page_type ='null'):
    ##passed variables default to null instead of none else it makes JavaScript freak out
    """
    config is used to configure certain elements/properties of the inputcomponent all variables result in pitchinput.js actions
    null skips the selected option (e.g. preloaded null will make the strikezone render empty)
    """
    
    input_config={}
    ##makes sure the inputcomponent only allows for one location to be tapped
    input_config['one_location'] = one_location
    ##accepts different preloaded zones to fill
    ##TODO paste format
    input_config['preloaded'] = preloaded
    ##enables you to disable certain buttons 
    input_config['disable_button'] = disable_button
    ##used for pagespecific oddities should be replaced by generic simple type (although keeping it task is nice and clear)
    input_config['task_log'] = task_log
    #unused?
    input_config['page_type'] = page_type
    return input_config

def format_str_date_to_year_month_day(date):
    #converts js dates to usable datetimes (comparable to db timestamps/started at)
    try:
        date = datetime.strptime(date, '%a %b %d %Y')
    except ValueError as v:
        ulr = len(v.args[0].partition('unconverted data remains: ')[2])
        if ulr:
            date = datetime.strptime(date[:-ulr], '%a %b %d %Y')
        else:
            raise v

    return date

#i hate this TODO make methods names for different js date parsing 
def format_str_date_to_year_month_day2(date):
    #converts js dates to usable datetimes now with a different format (comparable to db timestamps/started at)
    try:
        date = datetime.strptime(date, '%Y-%m-%d')
    except ValueError as v:
        ulr = len(v.args[0].partition('unconverted data remains: ')[2])
        if ulr:
            date = datetime.strptime(date[:-ulr], '%Y-%m-%d')
        else:
            raise v

    return date