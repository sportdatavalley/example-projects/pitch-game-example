import os
import warnings

debug_enabled = os.getenv('DB_DEBUG', 'False').lower() in ('true', '1')
db_user = os.getenv('DB_USERNAME')
db_pass = os.getenv('DB_USERPASS')
db_host = os.getenv('DB_HOST')
db_port = os.getenv('DB_PORT')
db_name = os.getenv('DB_NAME')

if not db_user:
  warnings.warn("Database config: user not set")
if not db_pass:
  warnings.warn("Database config: pass not set")
if not db_host:
  warnings.warn("Database config: host not set")
if not db_port:
  warnings.warn("Database config: port not set")
if not db_name:
  warnings.warn("Database config: name not set")

config = {
  'user': os.getenv('DB_USERNAME'),
  'password': os.getenv('DB_USERPASS'),
  'host': os.getenv('DB_HOST'),
  'database': os.getenv('DB_NAME'),
  'port': os.getenv('DB_PORT'),
  'raise_on_warnings': debug_enabled
}
