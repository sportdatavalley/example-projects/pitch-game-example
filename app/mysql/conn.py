import mysql.connector
from mysql.connector import errorcode
from .config import config
import os

def conn():
    try:
        conn = mysql.connector.connect(**config)
        return conn
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
            print("invalid username or password")
        elif err.errno == errorcode.ER_BAD_DB_ERROR:
            print("database not found")
        else:
            print(err)
    else:
        conn.close()