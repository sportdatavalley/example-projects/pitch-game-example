//verander naam later van dit dataobject -> throwingdataobject
//change list to throws
//var dataObject = {'list':[], 'throwtype':''};
//var taskDataObject = {'list':[], 'throwtype':'' , 'taskperformer':'','totalTries':'','totalNeeded':''}

$(document).ready(function () {
    $("#submitButton").click(function (e) {
        e.preventDefault();
        var dataObject = JSON
        var throwtypeKey = throwTypeTranslator(document.getElementById("summary-throw-type").textContent)
        dataObject['throwType'] = throwtypeKey
        dataObject['nrOfAttempts'] = document.getElementById("summary-throw-amount").textContent
        dataObject['nrOfHits'] = document.getElementById("summary-hits").textContent
        var dataString = JSON.stringify(dataObject)
        $.ajax({
            type: "POST",
            url: "/freethrow/log",
            data: dataString,
            success: function (msg) {
                document.getElementById("getDataObject").innerText = JSON.stringify({'list':[], 'throwtype':''});
                document.getElementById("close-modal").click()
                console.log("Working" + JSON.stringify(msg));
            },
            error: function (msg) {
                console.log('not working ' + JSON.stringify(msg));
            }
        });
        document.getElementById("getDataObject").innerText = JSON.stringify(dataObject)
        document.getElementById("strikeZoneImageInput").src = "/strikezone.svg";
    })
})

async function FillSummary()
{
    var dataObject = JSON.parse(document.getElementById("getDataObject").innerText)
    dataObject['throwtype'] = $('input[name=throwtype]:checked', '#throwtype_buttons').val()
    var dataString = JSON.stringify(dataObject)
    var summary

    await $.ajax({
        type: "GET",
        url: "/generateSummary",
        data: {
            "hitdata": dataString, 
        },
        success: function (msg) {
            summary = JSON.parse(msg)
        },
        error: function (msg) {
            console.log('not working ' + JSON.stringify(msg));
        }
    });
    

    var dataObject = JSON.parse(document.getElementById("getDataObject").innerText)

    var throwType = $('input[name=throwtype]:checked', '#throwtype_buttons').val()

    document.getElementById("summary-throw-amount").textContent = dataObject['list_of_throws'].length
    document.getElementById("summary-throw-type").textContent = throwType
    document.getElementById("summary-hits").textContent = summary['hits']
    document.getElementById("summary-accuracy").textContent = summary['accuracy'] +"%"
}


//TODO helper function
function throwTypeTranslator(throwType){
    if (throwType == 'fastball'){
        return 'FB'
    }
    if (throwType == 'curve'){
        return 'CB'
    }
    if (throwType == 'riseball'){
        return 'RB'
    }
    if (throwType == 'changeup'){
        return 'CUB'
    }
    if (throwType == 'drop'){
        return 'DB'
    }
    if (throwType == 'dropcurve'){
        return 'DCB'
    }
    if (throwType == 'screwball'){
        return 'SB'
    }
    if (throwType == 'other'){
        return 'other'
    }
    else return 'FB'
}