/*Database Script for pitchgame*/

/*create the database*/
CREATE DATABASE IF NOT EXISTS pitchgame;

/*use the pitchgame database*/
USE pitchgame;

/*create required tables*/
CREATE TABLE IF NOT EXISTS User (
    userId int NOT NULL auto_increment,
	teamname varchar(255),
    firstName varchar(255),
	lastName varchar(255),
    passwordHash varchar(255),
    emailAddress varchar(255),
    Primary Key(userId)
);

CREATE TABLE IF NOT EXISTS PitchType (
    code varchar(255) NOT NULL PRIMARY KEY,
	pitchType varchar(255)
);

CREATE TABLE IF NOT EXISTS Task (
	taskId int NOT NULL auto_increment PRIMARY KEY,
	taskDescription varchar(255),
	throwType varchar(255),
	nrOfAttempts int,
	percentageGoal int,
    FOREIGN KEY (throwType) REFERENCES PitchType(code)
);

CREATE TABLE IF NOT EXISTS Player (
	playerId int NOT NULL PRIMARY KEY,
	primaryPitchType varchar(255),
	playerLevel int,
    FOREIGN KEY (playerId) REFERENCES User(userId),
	FOREIGN KEY (primaryPitchType) REFERENCES PitchType(code)
);

CREATE TABLE IF NOT EXISTS PitchLogs (
	pitchLogId int NOT NULL auto_increment PRIMARY KEY,
	dateTime DATETIME,
	player int,
	pitchType varchar(255),
	pitchTask int,
	nrOfAttempts int,
	nrOfHits int,
    FOREIGN KEY(player) REFERENCES User(userId),
	FOREIGN KEY (pitchType) REFERENCES PitchType(code),
	FOREIGN KEY (pitchTask) REFERENCES Task(taskId)
);

CREATE TABLE IF NOT EXISTS TaskSet (
    taskSetId int NOT NULL auto_increment PRIMARY KEY,
	taskSetDescription varchar(255),
	taskLevel int,
	task1 int,
	task2 int,
	task3 int,
	task4 int,
    FOREIGN KEY(task1) REFERENCES Task(taskId),
	FOREIGN KEY(task2) REFERENCES Task(taskId),
	FOREIGN KEY(task3) REFERENCES Task(taskId),
	FOREIGN KEY(task4) REFERENCES Task(taskId)
);

/*insert scripts*/
INSERT INTO User (teamname, firstName, lastName, passwordHash, emailAddress)
VALUES ('Dutch National Team','John', 'Doe', '123password', 'email@fake.com');

INSERT INTO PitchType (code, pitchType)
VALUES
	('FB', 'fastball'),
    ('CB', 'curve'),
    ('RB', 'riseball'),
    ('CUB', 'changeup'),
    ('DB', 'drop'),
    ('DCB', 'dropcurve'),
	('SB', 'screwball'),
    ('Other	', 'other');

