from mysql.user import get_user, get_logs

##this method could use some loops for throwtypes
def get_pitch_accuracy(auth,userId):

    #light workaround to fix for users who dont have viewacces to themselves (get_user will fail for themselves)
    user = get_user(userId)

    #list of pitchtypes with numbers for accuracy
    #[0] total [1] hits [2] misses
    pitch_accuracy_calc = {'fastball':[0,0,0],'curve':[0,0,0],'rise':[0,0,0],'change-up':[0,0,0],'drop':[0,0,0],'drop-curve':[0,0,0],'screwball':[0,0,0],'other':[0,0,0]}
    pitch_accuracy = {'user':user['username'],'FB':0,'CB':0,'RB':0,'CUB':0,'DB':0,'DCB':0,'SB':0,'other':0}

    #get all sessions and events for users
    logs = get_logs(userId)


    #calculate total/hits/misses and place in dict
    pitch_accuracy_calc['fastball'] = pitch_total_calculator(events,'fastball')
    pitch_accuracy_calc['curve'] = pitch_total_calculator(events,'curve')
    pitch_accuracy_calc['rise'] = pitch_total_calculator(events,'riseball')
    pitch_accuracy_calc['change-up'] = pitch_total_calculator(events,'changeup')
    pitch_accuracy_calc['drop'] = pitch_total_calculator(events,'drop')
    pitch_accuracy_calc['drop-curve'] = pitch_total_calculator(events,'dropcurve')
    pitch_accuracy_calc['screwball'] = pitch_total_calculator(events,'screwball')
    pitch_accuracy_calc['other'] = pitch_total_calculator(events,'other')
    
    

    #calculates the percentages
    if pitch_accuracy_calc['fastball'][0] != 0:
        pitch_accuracy['FB'] = (pitch_accuracy_calc['fastball'][1] / pitch_accuracy_calc['fastball'][0]) * 100
    if pitch_accuracy_calc['curve'][0] != 0:
        pitch_accuracy['CB'] = (pitch_accuracy_calc['curve'][1] / pitch_accuracy_calc['curve'][0]) * 100
    if pitch_accuracy_calc['rise'][0] != 0:
        pitch_accuracy['RB'] =  (pitch_accuracy_calc['rise'][1] / pitch_accuracy_calc['rise'][0]) * 100
    if pitch_accuracy_calc['change-up'][0] != 0:
        pitch_accuracy['CUB'] = (pitch_accuracy_calc['change-up'][1] / pitch_accuracy_calc['change-up'][0]) * 100
    if pitch_accuracy_calc['drop'][0] != 0:
        pitch_accuracy['DB'] = (pitch_accuracy_calc['drop'][1] / pitch_accuracy_calc['drop'][0]) * 100
    if pitch_accuracy_calc['drop-curve'][0] != 0:
        pitch_accuracy['DCB'] = (pitch_accuracy_calc['drop-curve'][1] / pitch_accuracy_calc['drop-curve'][0]) * 100
    if pitch_accuracy_calc['screwball'][0] != 0:
        pitch_accuracy['SB'] = (pitch_accuracy_calc['screwball'][1] / pitch_accuracy_calc['screwball'][0]) * 100
    if pitch_accuracy_calc['other'][0] != 0:
        pitch_accuracy['other'] = (pitch_accuracy_calc['other'][1] / pitch_accuracy_calc['other'][0]) * 100
    

    #round percentages
    for key in pitch_accuracy.keys():
        if(key != 'user'):
            pitch_accuracy[key] = round(pitch_accuracy[key])
    return pitch_accuracy

def pitch_total_calculator(events,throwtype):
    pitch_accuracy_calc = [0,0,0]
    for event in events:
        if event.type != "task":
            if event.type == 'taskattempts':
                if event.data['throwtype'] == throwtype:
                    pitch_accuracy_calc[0] = pitch_accuracy_calc[0] + 1
                    if event.data['throw'] in event.data['target']:
                        pitch_accuracy_calc[1] = pitch_accuracy_calc[1] + 1
                    else:
                        pitch_accuracy_calc[2] = pitch_accuracy_calc[2] + 1
            if event.type =="free" or event.type =="level": 
                if event.data['throwtype'] == throwtype:
                    pitch_accuracy_calc[0] = pitch_accuracy_calc[0] + 1
                    if event.data['area'] in event.data['target']:
                        pitch_accuracy_calc[1] = pitch_accuracy_calc[1] + 1
                    else:
                        pitch_accuracy_calc[2] = pitch_accuracy_calc[2] + 1
    return pitch_accuracy_calc