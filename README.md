# Pitch-Game app
## Introduction
This is an app that serves as an example of how to use Docker/Docker-compose to set up the development environment + ease the deployment of the app.

## Requirements
You should have the following installed:
- Docker
- Docker-Compose

And nothing else ;)
All the necessary packages and languages will be installed inside the app's Docker image.

## Running

To start the development environment, go to the root folder of this app, and run:
```sh
docker-compose -f docker/docker-compose.yml up --build
```

This command will start all the services specified in the docker-compose file. In this case, it first starts the database service (called `mysqldb`) and then starts the app service (called `pitch-game`). A few notes on the parameters used:

- `-f` --> Specify which docker-compose file to use. (The default is the `docker-compose.yml` file in the current directory)
- `--build` --> Builds the app's image(s) before starting the container.

## Learning

To understand how you can set up Docker/Docker-compose, check the files inside the `docker/` folder. They have some comments that should help you understand what it is doing.