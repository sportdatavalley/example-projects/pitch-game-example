from .conn import conn

def get_user(userId):
    """
    returns user object for passed user id
    """
    db = conn()
    mycursor=db.cursor() 
    try:   
        mycursor.execute(f"SELECT * from User WHERE userId = '{userId}'") 
        account = mycursor.fetchone()
        db.close()
        return account
    except:    
        db.rollback()  
    db.close()

def get_logs(userId):
    """
    returns user object for passed user id
    """
    db = conn()
    mycursor=db.cursor() 
    try:   
        mycursor.execute(f"SELECT * from PitchType WHERE userId = '{userId}'") 
        logs = mycursor.fetchone()
        db.close()
        return logs
    except:    
        db.rollback()  
    db.close()