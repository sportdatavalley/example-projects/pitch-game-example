import json
import time
from random import randint
from urllib.parse import urlencode

from starlette.requests import Request
from starlette.responses import HTMLResponse, RedirectResponse, Response
from starlette.staticfiles import StaticFiles

from app.drawing import StrikeZone
from app import config
from app.config import templates

from fastapi import Cookie, Depends, FastAPI, APIRouter

router = APIRouter()  

@router.get('/generateSummary')
async def generateSummary(request: Request):

    "Generates a post session summary with accuracy data"
    throws = request.query_params['hitdata']
    throw_JSON = json.loads(throws)
    ##todo make helper method
    throws_dict = {}
    areas = []
    for t in throw_JSON['list_of_throws']:
        area = StrikeZone().find_area(float(t['x']), float(t['y']))

        if not any(elem['area'] == area for elem in areas):
            areas.append({
                'area': area,
            })
            throws_dict[area] = 1
        else:
            for a in areas:
                if a['area'] == area:
                    throws_dict[area] = throws_dict[area] + 1
                    continue
                    
    
    targets_dict = {}
    areas = []

    #if the data comes from task page target is already a letter so it does not need to be translated
    #TODO test for more than 1 target zone
    for t in throw_JSON['target']:
        if isinstance(t,str):
            targets_dict[t] = 1
        else:
            area = StrikeZone().find_area(float(t['x']), float(t['y']))

            if not any(elem['area'] == area for elem in areas):
                areas.append({
                    'area': area,
                })
                targets_dict[area] = 1
            
    
    #throws dict is letter and amount of location is hit {a:1 b:2}
    #targets dict is letter and if location is target {a:1}

    hits = 0 
    misses = 0 
    total = 0 
    
    for throw_key in throws_dict:
        if throw_key in targets_dict.keys():
            hits = hits + throws_dict[throw_key]
            total = total + throws_dict[throw_key]
        else:
            #miss
            # a miss has occured in order to not register a miss twice (e.g a throw misses twice with 2 targets)
            # we must take the throw and ?? 
            # i dont remember
            misses = misses + throws_dict[throw_key]
            total = total + throws_dict[throw_key]

    
    summary = {}
    summary['accuracy'] = round((hits/total)*100,2)
    summary['hits'] = hits
    summary['misses'] = misses
    summary['total'] = total

    summary_response = json.dumps(summary)
    summary_response_str = str(summary_response)
    return HTMLResponse(summary_response_str)    

