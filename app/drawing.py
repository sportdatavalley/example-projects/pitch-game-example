import decimal
from io import BytesIO

import cairo

WIDTH = 100
HEIGHT = 100
TOTAL_X = 1000
TOTAL_Y = 1000

THICKNESS_OUTER = 1 / 8
THICKNESS_INNER = 2 / 8


class SizeException(Exception):

    def __init__(self, message):
        self.message = "\nSize error: " + message + "\n"


class StrikeZone:
    ##new colors
    GRAY_BACKGROUND = (247, 243, 245)
    ##end new colors

    RED = (1, 0, 0)
    ORANGE = (1, 165 / 255, 0)
    YELLOW = (1, 1, 0)
    GREEN = (0, 1, 0)
    GRAY = (247, 243, 245)
    BLUE = (0, 0, 1)
    SDV_LIGHT_BLUE = (26 / 255, 183 / 255, 234 / 255)
    SDV_DARK_BLUE = (1 / 255, 20 / 255, 96 / 255)
    SDV_ORANGE = (226 / 255, 74 / 255, 15 / 255)
    WHITE = (255 / 255, 255 / 255, 255 / 255)
    BLACK = (0, 0, 0)
    LINECOLOR = BLACK

    def __init__(self, scores=None, highlights=None, markers=None,
                 correctareas=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i'],
                 target_area = None):
        self.scores = scores if scores is not None else {}
        self.highlights = highlights if highlights is not None else []
        self.markers = markers if markers is not None else []
        self.correctareas = correctareas if correctareas is not None else []
        self.target_area = target_area if target_area is not None else []

    def draw(self):
        """
        draws the strikezone
        """
        svgio = BytesIO()
        with cairo.SVGSurface(svgio, TOTAL_X, TOTAL_Y) as surface:
            self.context = cairo.Context(surface)
            self.context.scale(TOTAL_X, TOTAL_Y)
            for area in 'abcdefghijklm':
                # for area in 'abcdefghijklm':
                self._render(area)

            self._draw_markers()
        return svgio

    def find_area(self, x, y):
        """
        returns the area that is in the passed coordinates, (a, b, c etc.)
        """
        y = y / TOTAL_X * TOTAL_Y
        svgio = BytesIO()
        with cairo.SVGSurface(svgio, TOTAL_X, TOTAL_Y) as surface:
            self.context = cairo.Context(surface)
            self.context.scale(TOTAL_X, TOTAL_Y)
            for area in 'abcdefghijklm':
                # for area in 'abcdefghijklm':
                getattr(self, f'_draw_{area}')()
                if self.context.in_fill(x, y):
                    break
                self.context.fill()
        return area

    def _render(self, area):
        """
        render an area
        """
        getattr(self, f'_draw_{area}')()
        self.context.set_source_rgb(*self._get_fill_color(area))
        self.context.fill()

        getattr(self, f'_draw_{area}')()
        self.context.set_line_width(0.005)
        self.context.set_source_rgb(
            self.LINECOLOR[0], self.LINECOLOR[1], self.LINECOLOR[2])
        self.context.stroke()

        try:
            getattr(self, f'_draw_text_{area}')()
        except AttributeError:
            pass

    def _draw_markers(self):
        """
        draw the markers in self.markers
        """
        for loc_x, loc_y in self.markers:
            loc_y = loc_y / TOTAL_X * TOTAL_Y

            self.context.set_source_rgb(*self.SDV_DARK_BLUE)
            font_size = 0.04
            self.context.set_font_size(font_size)
            self.context.select_font_face(
                'OpenMoji',
                cairo.FONT_SLANT_NORMAL,
                cairo.FONT_WEIGHT_BOLD)

            self.context.move_to(loc_x - 0.021, loc_y + 0.013)
            self.context.text_path('\U0001f3c0')
            self.context.fill()

    def _get_fill_color(self, area):
        """
        determines fill colour based on if area is in a correct area.
        """
        if area in self.highlights:
            if area in self.target_area:
                return self.GREEN
            if area in self.correctareas:
                return self.YELLOW
            else:
                return self.RED

        return self.GRAY_BACKGROUND

    def _calculate_percentage(self, score, count):
        return decimal.Decimal(score / count * 100).quantize(decimal.Decimal("1"), decimal.ROUND_HALF_UP)

    def _get_rgb(self, colors, percentage):
        """ This method will calculate from which colors the rgb value
        should be calculated.

        Based on the length of the array of colors, this method will
        calculate from which two colors the gradient should be defined
        and which colors the rgb array should have.

        @param {array} [colors]     =   array of tuples of colors: (r,g,b)
        @param {int} [percentage]   =   percentage from the pitch area
        @return {array}             =   returns an array of three values
                                        between 0-1: [r,g,b]
        """
        size = len(colors)
        # print("Size of color array:", size)
        try:
            if (size < 1):
                raise SizeException("Array size of colors is too small.")
            elif (size == 1):
                return [colors[0][0], colors[0][1], colors[0][2]]
            elif (size == 2):
                return self._get_gradient_rgb(colors[0], colors[1], percentage)
            else:
                if percentage == 100:
                    # Percentage of 100 will receive calculated percentage of 0
                    # in the section after the last section. This is not usable
                    # so just first check for a percentage of 100.
                    return self._get_gradient_rgb(colors[(size - 2)], colors[(size - 1)], percentage)
                else:
                    # size - 1: with for example three colors there are 2 gradients needed
                    section = float(100 / (size - 1))
                    # print("Section size:", section)
                    offset = percentage % section
                    # print("Offset:", offset)
                    first_color_index = round(
                        (percentage - offset) / section)
                    if first_color_index < 0:
                        first_color_index = 0
                    second_color_index = first_color_index + 1
                    calculated_perc = (offset * (100 / section))
                    # print("Calc perc:", calculated_perc)
                    return self._get_gradient_rgb(colors[first_color_index], colors[second_color_index],
                                                  calculated_perc)
        except SizeException as err:
            print(err.message)
            return False

    def _get_gradient_rgb(self, color1, color2, percentage):
        """ This method will calculate the actual rgb values
        based on two colors and the percentage.

        In a range of 0-100 the values of the red, green and blue
        channel will be calculated between 0-1, based on the
        percentage that's given.

        @param {tuple} [color1]     =   the first color
        @param {tuple} [color2]     =   the second color
        @param {int} [percentage]   =   relative percentage within the
                                        range of the gradient.
        @return {array}             =   returns an array of three values
                                        between 0-1: [r,g,b]
        """

        inverse_perc = 100 - percentage
        perc_parsed = percentage / 100
        inv_perc_parsed = inverse_perc / 100

        rgb = [
            (color1[0] * inv_perc_parsed) + (color2[0] * perc_parsed),
            (color1[1] * inv_perc_parsed) + (color2[1] * perc_parsed),
            (color1[2] * inv_perc_parsed) + (color2[2] * perc_parsed)
        ]

        # print("RGB:", rgb)
        return rgb

    def _draw_text(self, area, x, y):
        """
        draws text in an area or on specific coordinates
        """
        if self.scores is None:
            return
        try:
            value = self.scores[area]
        except (KeyError, AttributeError):
            return

        self.context.set_source_rgb(0, 0, 0)
        self.context.select_font_face(
            'Arial',
            cairo.FONT_SLANT_NORMAL,
            cairo.FONT_WEIGHT_BOLD)

        self.context.move_to(x, y)
        self.context.set_font_size(0.035)
        self.context.text_path(str(value))
        self.context.fill()

    # ######################## #
    #          TARGET          #
    # ######################## #

    # coords
    def _x_mid_left_coord(self):
        return 0.0, 0.5

    def _x_mid_fifth_coord(self):
        return THICKNESS_OUTER, 0.5

    def _x_mid_four_fifth_coord(self):
        return (1 - THICKNESS_OUTER), 0.5

    def _x_mid_right_coord(self):
        return 1, 0.5

    def _y_mid_top_coord(self):
        return 0.5, 0.0

    def _y_mid_fifth_coord(self):
        return 0.5, THICKNESS_OUTER

    def _y_mid_four_fifth_coord(self):
        return 0.5, (1 - THICKNESS_OUTER)

    def _y_mid_bottom_coord(self):
        return 0.5, 1.0

    def _inner_top_left_coord(self):
        return THICKNESS_OUTER, THICKNESS_OUTER

    def _inner_top_right_coord(self):
        return (1 - THICKNESS_OUTER), THICKNESS_OUTER

    def _inner_bottom_left_coord(self):
        return THICKNESS_OUTER, (1 - THICKNESS_OUTER)

    def _inner_bottom_right_coord(self):
        return (1 - THICKNESS_OUTER), (1 - THICKNESS_OUTER)

    # Inner target
    def _draw_a(self):
        self.context.rectangle(
            THICKNESS_OUTER, THICKNESS_OUTER, THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_a(self):
        self._draw_text('a', 25 / WIDTH, 25 / HEIGHT)

    def _draw_b(self):
        self.context.rectangle(
            (THICKNESS_OUTER + (1 * THICKNESS_INNER)), THICKNESS_OUTER, THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_b(self):
        self._draw_text('b', 50 / WIDTH, 25 / HEIGHT)

    def _draw_c(self):
        self.context.rectangle((THICKNESS_OUTER + (2 * THICKNESS_INNER)),
                               THICKNESS_OUTER, THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_c(self):
        self._draw_text('c', 75 / WIDTH, 25 / HEIGHT)

    def _draw_d(self):
        self.context.rectangle(THICKNESS_OUTER, (THICKNESS_OUTER +
                                                 (1 * THICKNESS_INNER)), THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_d(self):
        self._draw_text('d', 25 / WIDTH, 50 / HEIGHT)

    def _draw_e(self):
        self.context.rectangle((THICKNESS_OUTER + (1 * THICKNESS_INNER)),
                               (THICKNESS_OUTER + (1 * THICKNESS_INNER)), THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_e(self):
        self._draw_text('e', 50 / WIDTH, 50 / HEIGHT)

    def _draw_f(self):
        self.context.rectangle((THICKNESS_OUTER + (2 * THICKNESS_INNER)),
                               (THICKNESS_OUTER + (1 * THICKNESS_INNER)), THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_f(self):
        self._draw_text('f', 75 / WIDTH, 50 / HEIGHT)

    def _draw_g(self):
        self.context.rectangle(THICKNESS_OUTER, (THICKNESS_OUTER +
                                                 (2 * THICKNESS_INNER)), THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_g(self):
        self._draw_text('g', 25 / WIDTH, 75 / HEIGHT)

    def _draw_h(self):
        self.context.rectangle((THICKNESS_OUTER + (1 * THICKNESS_INNER)),
                               (THICKNESS_OUTER + (2 * THICKNESS_INNER)), THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_h(self):
        self._draw_text('h', 50 / WIDTH, 75 / HEIGHT)

    def _draw_i(self):
        self.context.rectangle((THICKNESS_OUTER + (2 * THICKNESS_INNER)),
                               (THICKNESS_OUTER + (2 * THICKNESS_INNER)), THICKNESS_INNER, THICKNESS_INNER)

    def _draw_text_i(self):
        self._draw_text('i', 75 / WIDTH, 75 / HEIGHT)

    # Outer target
    # outer top left
    def _draw_j(self):
        self.context.move_to(0.0, 0.0)
        self.context.line_to(*self._y_mid_top_coord())
        self.context.line_to(*self._y_mid_fifth_coord())
        self.context.line_to(*self._inner_top_left_coord())
        self.context.line_to(*self._x_mid_fifth_coord())
        self.context.line_to(*self._x_mid_left_coord())
        self.context.line_to(0.0, 0.0)

    def _draw_text_j(self):
        self._draw_text('j', 5 / WIDTH, 5 / HEIGHT)

    # outer top right
    def _draw_k(self):
        self.context.move_to(*self._y_mid_top_coord())
        self.context.line_to(1.0, 0.0)
        self.context.line_to(*self._x_mid_right_coord())
        self.context.line_to(*self._x_mid_four_fifth_coord())
        self.context.line_to(*self._inner_top_right_coord())
        self.context.line_to(*self._y_mid_fifth_coord())
        self.context.line_to(*self._y_mid_top_coord())

    def _draw_text_k(self):
        self._draw_text('k', 90 / WIDTH, 5 / HEIGHT)

    # outer bottom left
    def _draw_l(self):
        self.context.move_to(*self._x_mid_left_coord())
        self.context.line_to(*self._x_mid_fifth_coord())
        self.context.line_to(*self._inner_bottom_left_coord())
        self.context.line_to(*self._y_mid_four_fifth_coord())
        self.context.line_to(*self._y_mid_bottom_coord())
        self.context.line_to(0.0, 1.0)
        self.context.line_to(*self._x_mid_left_coord())

    def _draw_text_l(self):
        self._draw_text('l', 5 / WIDTH, 95 / HEIGHT)

    # outer bottom right
    def _draw_m(self):
        self.context.move_to(*self._y_mid_four_fifth_coord())
        self.context.line_to(*self._inner_bottom_right_coord())
        self.context.line_to(*self._x_mid_four_fifth_coord())
        self.context.line_to(*self._x_mid_right_coord())
        self.context.line_to(1.0, 1.0)
        self.context.line_to(*self._y_mid_bottom_coord())
        self.context.line_to(*self._y_mid_four_fifth_coord())

    def _draw_text_m(self):
        self._draw_text('m', 90 / WIDTH, 95 / HEIGHT)


def draw_strikezone():
    svgio = StrikeZone(markers=[(0.5, 0.5)]).draw()
    return svgio


if __name__ == '__main__':
    svgio = draw_strikezone()
    with open('pitch.svg', 'wb') as f:
        f.write(svgio.getvalue())
