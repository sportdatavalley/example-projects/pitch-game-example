from datetime import datetime, timedelta

from pydantic import BaseModel, root_validator


class Token(BaseModel):
    access_token: str
    expires_in: int
    token_type: str
    scope: str
    refresh_token: str
    expires_at: str = None

    @root_validator(pre=True)
    def convert_expires_in_to_datetime(cls, values):
        if values.get('expires_at', None) is None:
            expires_at = datetime.now() + timedelta(seconds=values['expires_in'])
            values['expires_at'] = expires_at.isoformat()

        return values


class Session(BaseModel):
    id: str
    user: str


class Event(BaseModel):
    id: str
    timestamp: datetime
    session: str
    type: str
    data: dict


class User(BaseModel):
    id: str
    username: str


class Auth(BaseModel):
    token: Token
    user: User = None
