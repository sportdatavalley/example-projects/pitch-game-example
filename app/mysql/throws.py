from .conn import conn
from datetime import datetime

def log_throws(userId, pitchType, nrOfAttempts, nrOfHits):
   db=conn()
   mycursor=db.cursor()   
   try:   
    mycursor.execute(f"INSERT INTO PitchLogs (dateTime, player, pitchType, nrOfAttempts, nrOfHits) values(now(),{userId},'{pitchType}',{nrOfAttempts},{nrOfHits})")  
    db.commit()
    return 'Successfully Logged!'
   except:    
      db.rollback()  
   db.close()

def get_throws(userId):
   db=conn()
   mycursor=db.cursor()   
   try:   
      mycursor.execute(f"SELECT * from PitchLogs WHERE player = {userId}")  
      logs = mycursor.fetchall()
      return logs 
   except:    
      db.rollback()  
   db.close()

def get_throws_by_type(userId, pitchType):
   db=conn()
   mycursor=db.cursor()   
   try:   
      mycursor.execute(f"SELECT * from PitchLogs WHERE player = {userId} AND pitchType = {pitchType}")  
      logs = mycursor.fetchall()
      return logs 
   except:    
      db.rollback()  
   db.close()

def get_tasks(userId):
   db=conn()
   mycursor=db.cursor()   
   try:   
      mycursor.execute(f"SELECT * from Task WHERE(player = {userId}")  
      logs = mycursor.fetchall()
      return logs 
   except:    
      db.rollback()  
   db.close()