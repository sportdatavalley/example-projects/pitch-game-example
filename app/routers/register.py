#basic imports
import json
from random import randint
from urllib.parse import urlencode
from _datetime import datetime
from typing import Optional
import hashlib


#application imports
from app import config
from app.config import templates
from app.mysql.login import register_db

#request imports
from starlette.requests import Request
from starlette.responses import HTMLResponse, RedirectResponse, Response
from starlette.staticfiles import StaticFiles

#api imports
from fastapi import APIRouter, Cookie, Depends, FastAPI, Form

router = APIRouter()

@router.get('/', response_class=HTMLResponse)
async def free_throw(request: Request):
    return templates.TemplateResponse(
        name='register.tmpl',
        context={
            'request': request,
            'page': 'Register',
        }
    )

@router.post('/register/', response_class=HTMLResponse)
async def register(request= Request, teamname: str = Form(...), firstName: str = Form(...) , lastName: str = Form(...) , username: str = Form(...), password: str = Form(...)):
    encode = password.encode('utf-8')
    hashedPass = hashlib.sha224(encode).hexdigest()
    user = register_db(teamname,firstName ,lastName, username, hashedPass)
    return user