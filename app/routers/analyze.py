#basic imports
import json
from random import randint
from urllib.parse import urlencode
from _datetime import datetime
from typing import Optional


#application imports
from app import config
from app.config import templates
from app.helpers import Helper

#request imports
from starlette.requests import Request
from starlette.responses import HTMLResponse, RedirectResponse, Response
from starlette.staticfiles import StaticFiles

#api imports
from fastapi import APIRouter, Cookie, Depends, FastAPI

router = APIRouter()

@router.get('/', response_class=HTMLResponse)
async def analyze(request: Request, id: Optional[str] = Cookie(None)):
    
    input_config = Helper.build_input_config()
    return templates.TemplateResponse(
        name='analyze.tmpl',
        context={
            'id': id,
            'request': request,
            'page': 'Analyze',
            'config': config,
        }
    )


