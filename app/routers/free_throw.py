#basic imports
import json
from random import randint
from urllib.parse import urlencode
from _datetime import datetime
from typing import Optional

#application imports
from app import config
from app.config import templates
from app.drawing import StrikeZone
from app.helpers import Helper
from app.mysql.throws import log_throws

#request imports
from starlette.requests import Request
from starlette.responses import HTMLResponse, RedirectResponse, Response
from starlette.staticfiles import StaticFiles

#api imports
from fastapi import APIRouter, Cookie, Depends, FastAPI, Form

router = APIRouter()

@router.get('/', response_class=HTMLResponse)
async def free_throw(request: Request, id: Optional[str] = Cookie(None)):
    
    input_config = Helper.build_input_config()
    return templates.TemplateResponse(
        name='freethrow.tmpl',
        context={
            'id': id,
            'request': request,
            'page': 'Free Throw',
            'input_component_config': input_config,
            'config': config,
        }
    )

@router.post('/log/')
async def log(*, throws: dict, request= Request, id: Optional[str] = Cookie(None)):

    playerId = id
    pitchType = (throws['throwType'])
    nrOfAttempts = (throws['nrOfAttempts'])
    nrOfHits = (throws['nrOfHits'])
    print(playerId, pitchType, nrOfAttempts, nrOfHits)
    log_throws(playerId, pitchType, nrOfAttempts, nrOfHits)
    return "throws logged"

