from starlette.config import Config
from starlette.templating import Jinja2Templates

config = Config(".env")

DEVELOPMENT = config('DEVELOPMENT', cast=bool, default=False)

templates = Jinja2Templates(directory='templates')
