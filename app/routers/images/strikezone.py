#basic imports
import json
from random import randint
from urllib.parse import urlencode

#application imports
from app import config
from app.config import templates
from app.schemas import Auth
from app.drawing import StrikeZone
from app.helpers import Helper

#request imports
from starlette.requests import Request
from starlette.responses import HTMLResponse, RedirectResponse, Response
from starlette.staticfiles import StaticFiles

#api imports
from fastapi import APIRouter, Cookie, Depends, FastAPI

router = APIRouter()

@router.get('/strikezone.svg')
async def strikezone(a: str = None, b: str = None, c: str = None,
                      d: str = None, e: str = None, f: str = None,
                      g: str = None, h: str = None, i: str = None,
                      j: str = None, k: str = None, l: str = None,
                      m: str = None, loc: str = None, highlight: str = None, random: bool = False,
                      target_area : str = None, preloaded : str = None
                      ):
    """
    Returns a strikezone image based on passed areas that need to be coloured, highlighted,
    or where a figure should be drawn(loc), can also draw random combinations
    """
    scores = {}
    highlights = []
    target_areas = []
    if preloaded is not None and preloaded != "null":
        preload_JSON = json.loads(preloaded.replace("'", '"'))
        for location in preload_JSON:
            highlights.append(location)
            target_areas.append(location)

    #highlights means throws
    if highlight is not None:
        hl = json.loads(highlight)
        for t in hl:
            area = StrikeZone().find_area(float(t['x']), float(t['y']))

            if area in scores:
                scores[area] += 1
            else:
                scores[area] = 1

            highlights.append(area)
    
    
    if target_area is not None:
        target_areaJSON = json.loads(target_area)
        if target_areaJSON != '':
            for target in target_areaJSON:
                area = StrikeZone().find_area(float(target['x']), float(target['y']))
                highlights.append(area)
                target_areas.append(area)

    for area in 'abcdefghijklm':
        value = locals()[area]
        if value is not None:
            try:
                scores[area] = value
                highlights.append(area)
            except (TypeError, ValueError):
                continue

    markers = []
    if loc is not None:
        loc_x, loc_y = (float(l) for l in loc.split(','))
        markers.append((loc_x, loc_y))
        area = StrikeZone().find_area(loc_x, loc_y)
        highlights.append(area)

    #Random highlights with random number of hits
    if random:
        for area in 'abcdefghijklm':
            count = randint(10, 100)
            score = count - randint(1, count - 1)
            scores[area] = (score, count)
    svgio = StrikeZone(scores=scores, highlights=highlights,
                       markers=markers,target_area=target_areas).draw()
    content = svgio.getvalue().decode('utf-8')

    return Response(content=content, media_type='image/svg+xml')

@router.get('/strikezone_analyze')
async def strikezone_analyze(
                            throwtype: str = None, 
                            from_date: str =None,
                            to_date: str =None,
                            id: str = None
                            ):
    """
    returns redirects to a strikezone image based on passed sessions and throwtype
    """
    from_date = Helper.format_str_date_to_year_month_day(from_date)
    to_date = Helper.format_str_date_to_year_month_day(to_date)
    
    print('here')

    #get all date selected sessions
    sessions = realtime_session.get_sessions_for_user(auth,id)
    filtered_sessions = []
    for session in sessions:
        start_date = session.started_at.replace(tzinfo=None)
        #session_date = datetime.fromisoformat(session.started_at)
        if start_date >= from_date and start_date <= to_date:
            filtered_sessions.append(session)
    
    sessions = [s.id for s in filtered_sessions]
    
    """
    workaround for leading whitespace in throwtype
    """
    throwtype_stripped = throwtype.strip()

    summary = get_analysis_for_sessions(auth,sessions,throwtype_stripped)
    summary['auth'] = auth
    summary['throwtype'] = throwtype_stripped

    return RedirectResponse(f'/strikezone.svg?{urlencode(summary)}')

def get_analysis_for_sessions(auth, session_ids, throwtype):
    """
    gets all event for a session and returns it in a format that can be processed client side into the analysis strikezone
    """
    events = realtime_event.get_events_for_sessions(auth,session_ids)
    summary = {}

    
    for event in events:
        #processing data for taskattempts
        if event.type == 'taskattempts':
            if 'throwtype' in event.data:
                if throwtype is None or throwtype is "" or event.data['throwtype'].strip() == throwtype:
                    target = event.data['target']
                    throw = event.data['throw']
                    
                    'check if key (area) exists in summary if not create it and add the total and hit parameter'
                    'results in summary = {a:{hit:0,total:1}}'
                    if target not in summary:
                        summary[target] = {'total': 1}
                        if target is throw:
                            summary[target]['hit'] = 1
                        else:
                            summary[target]['hit'] = 0
                    else:
                        if target is throw:
                            newHit = summary[target]['hit'] +1
                            summary[target]['hit'] = newHit
                        newTotal = summary[target]['total'] + 1
                        summary[target]['total'] = newTotal
        #processing data for free logging + level
        if event.type == 'free' or event.type == 'level':
            if 'throwtype' in event.data:
                if throwtype is None or throwtype is "" or event.data['throwtype'].strip() == throwtype:
                    targets = event.data['target']
                    ##TODO can use same if method of task if this changes to ['data']['throw']
                    throw = event.data['area']
                    'check if key (area) exists in summary if not create it and add the total and hit parameter'
                    'results in summary = {a:{hit:0,total:1}}'
                    if len(targets) == 1:
                        for target in targets:
                            if target not in summary:
                                summary[target] = {'total': event.data['amount']}
                                if target is throw:
                                    summary[target]['hit'] = event.data['amount']
                                else:
                                    summary[target]['hit'] = 0
                            else:
                                if target is throw:
                                    newHit = summary[target]['hit'] + event.data['amount']
                                    summary[target]['hit'] = newHit
                                newTotal = summary[target]['total'] + event.data['amount']
                                summary[target]['total'] = newTotal
                    else:
                        for target in targets:
                            if target not in summary:
                                summary[target] = {'total': event.data['amount']}
                                if throw in targets:
                                    summary[target]['hit'] = event.data['amount']
                                else:
                                    summary[target]['hit'] = 0
                            else:
                                if throw in targets:
                                    newHit = summary[target]['hit'] + event.data['amount']
                                    summary[target]['hit'] = newHit
                                newTotal = summary[target]['total'] + event.data['amount']
                                summary[target]['total'] = newTotal
    for key, value in summary.items():
        summary[key] = (summary[key]['hit'] / summary[key]['total'])*100
    for key in summary:
        summary[key] = str(round(summary[key]))+" %"
    return summary
