$( document ).ready(function() {
    jQuery('#getConfig').click();
});

function GetConfig(input_config)
{
    document.getElementById("configText").innerText = JSON.stringify(input_config)
    var config = input_config
    if(config['disable_button']==='hit_location'){
        document.getElementById("hit_button").style.display ='none'
        document.getElementById("getInputMode").innerText = "target"
    }
    else if(config['disable_button']==='target_location'){
        document.getElementById('target_button').style.display ='none'
        document.getElementById("getInputMode").innerText = "hit"
    }
    else{
        document.getElementById("getInputMode").innerText = "hit"
    }
    
    config = JSON.parse(document.getElementById("configText").innerText)

    preloadedZone = JSON.stringify(config['preloaded'])

    
}


function resetCourt(image) {
    
    var config = JSON.parse(document.getElementById("configText").innerText)
    var preloadedZone = JSON.stringify(config['preloaded'])

    var dataObject = JSON.parse(document.getElementById("getDataObject").innerText)
    
    if (config['page_type']==='level'){
        //method in level_challenge.js
        levelDisplayCourt(dataObject,config)
        
    }
    else if (preloadedZone === '"null"'){
        image.src = "/strikezone.svg?highlight=" + JSON.stringify(dataObject['list_of_throws'])+"&target_area="+JSON.stringify(dataObject['target'])
    }
    else{
        image.src = "/strikezone.svg?highlight=" + JSON.stringify(dataObject['list_of_throws'])+"&target_area="+JSON.stringify(dataObject['target'])+"&preloaded="+preloadedZone
    }
}

function FindPosition(oElement) {
    if (typeof (oElement.offsetParent) != "undefined") {
        for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
            posX += oElement.offsetLeft;
            posY += oElement.offsetTop;
        }
        return [posX, posY];
    } else {
        return [oElement.x, oElement.y];
    }
}



function GetCoordinates(e) {
    var inputMode = document.getElementById("getInputMode").innerText
    var dataObject = JSON.parse(document.getElementById("getDataObject").innerText)
    var config = JSON.parse(document.getElementById("configText").innerText)
    var preloadedZone = JSON.stringify(config['preloaded'])
    //get preloaded zones from query param
    if(preloadedZone == null){
        var url = new URL(document.getElementById("strikeZoneImageInput").src);
        preloadedZone = url.searchParams.get("preloaded");
    }
    //To make sure if one location is active (relevant for task adding page)
    if(config['one_location'] != 'null'){
        dataObject['list_of_throws'] = []
    }
    
    //do some input capturing (click location to x/y coordinates)
    var PosX = 0;
    var PosY = 0;
    var ImgPos;
    var image = document.getElementById("strikeZoneImageInput");
    ImgPos = FindPosition(image);
    if (!e) var e = window.event;
    if (e.pageX || e.pageY) {
        PosX = e.pageX;
        PosY = e.pageY;
    } else if (e.clientX || e.clientY) {
        PosX = e.clientX + document.body.scrollLeft +
            document.documentElement.scrollLeft;
        PosY = e.clientY + document.body.scrollTop +
            document.documentElement.scrollTop;
    }
    PosX = ((PosX - ImgPos[0]) / image.clientWidth).toFixed(10);
    PosY = ((PosY - ImgPos[1]) / image.clientHeight).toFixed(10);

    //error preventing
    if(dataObject['list_of_throws']== undefined){
        dataObject['list_of_throws'] = []
    }
    if(dataObject['target']== undefined || dataObject['target']== ""){
        
        dataObject['target'] = []
    }

    //mode switching (must the tap be logged under as a hit or target location)
    if(inputMode == "hit"){
        dataObject['list_of_throws'].push({x: PosX, y: PosY});
    }
    if(inputMode == "target"){
        dataObject['target'].push({x: PosX, y: PosY})
    }

    //resetcourt contains logic how a strikezone should be displayed on a specific page
    document.getElementById("getDataObject").innerText = JSON.stringify(dataObject)
    resetCourt(image)
    

    //To trigger the code in task_log for keeping track of player score
    if(config['task_log'] !='null' && inputMode == "hit"){
        //method in task_log.js
        UpdateTaskTracker(dataObject);
    }
}



//TODO could all be one function with a passed paramter of what button it is
function SwitchEraserMode(){
    var image = document.getElementById("strikeZoneImageInput");
    document.getElementById("getDataObject").innerText = JSON.stringify({'target':'','list_of_throws':[], 'throwtype':''})
    resetCourt(image);
    //method in task log
    resetTaskCounter()
}
function SwitchTargetMode(){
    var inputMode = document.getElementById("getInputMode").innerText
    var targetButton = document.getElementById("target_button")
    var hitButton = document.getElementById("hit_button")
    var config = JSON.parse(document.getElementById("configText").innerText)
    if(config['disable_button']=== 'null'){
        if (inputMode !== "target")
        {
            document.getElementById("getInputMode").innerText = "target";
            targetButton.className = "btn btn-success btn-lg btn-hit";
            hitButton.className = "btn btn-warning btn-lg btn-target btn-target-active"
        }
        else{
            document.getElementById("getInputMode").innerText = "hit";
            targetButton.className = "btn btn-success btn-lg btn-hit btn-hit-active";
            hitButton.className = "btn btn-warning btn-lg btn-target"
        }
    }
}
function SwitchHitMode(){
    var inputMode = document.getElementById("getInputMode").innerText
    var targetButton = document.getElementById("target_button")
    var hitButton = document.getElementById("hit_button")
    var config = JSON.parse(document.getElementById("configText").innerText)
    if(config['disable_button']=== 'null'){
        if (inputMode !== "hit")
        {
            document.getElementById("getInputMode").innerText = "hit"
            targetButton.className = "btn btn-success btn-lg btn-hit btn-hit-active";
            hitButton.className = "btn btn-warning btn-lg btn-target"
        }
        else{
            document.getElementById("getInputMode").innerText = "target"
            hitButton.className = "btn btn-warning btn-lg btn-target"
            targetButton.className = "btn btn-success btn-lg btn-hit btn-hit-active";
        }
    }
}