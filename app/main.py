import hashlib
import json

from random import randint
from urllib.parse import urlencode
import os
from starlette.requests import Request
from starlette.staticfiles import StaticFiles
from typing import Optional

from app.drawing import StrikeZone
from . import config
from .config import templates
from .schemas import Session
from .routers import analyze, free_throw, retrieve_data
from .routers.images import strikezone

from .mysql.login import login_db, register_db

from fastapi.security import OAuth2PasswordRequestForm
from fastapi import FastAPI, Depends, status, Form, Cookie
from fastapi.responses import RedirectResponse,HTMLResponse

app = FastAPI()

app.mount("/static", StaticFiles(directory="static"), name="static")

app.include_router(retrieve_data.router)
app.include_router(strikezone.router)
app.include_router(
    analyze.router,
    prefix='/analyze')

app.include_router(
    free_throw.router,
    prefix='/freethrow')

app.include_router(
    free_throw.router,
    prefix='/register')


@app.get('/', response_class=HTMLResponse)
async def landing_page(request: Request, id: Optional[str] = Cookie(None)):
    """
        return the home template as HTMLResponse
    """      
    return templates.TemplateResponse(
            name='home.tmpl',
            context={
                'id': id,
                'request': request,
                'page': 'home',
                'config': config
            })


@app.post('/login/', response_class=HTMLResponse)
async def login(request= Request, username: str = Form(...), password: str = Form(...)):
    encode = password.encode('utf-8')
    hashedPass = hashlib.sha224(encode).hexdigest()
    user = login_db(username, hashedPass)

    if user is None:
        return "login failed"
    else:
        for row in user:
            response = RedirectResponse('/')
            response.set_cookie(key='id', value=str(user[0]))
            response.status_code = 302
            return response

@app.get('/logout')
async def logout():

    response = RedirectResponse('/')
    response.delete_cookie(key="id")
    return response


@app.post('/register/', response_class=HTMLResponse)
async def register(request= Request, teamname: str = Form(...), firstName: str = Form(...) , lastName: str = Form(...) , username: str = Form(...), password: str = Form(...)):
    encode = password.encode('utf-8')
    hashedPass = hashlib.sha224(encode).hexdigest()
    register_db(teamname,firstName ,lastName, username, hashedPass)
    
    response = RedirectResponse('/')
    response.status_code = 302
    return response
    