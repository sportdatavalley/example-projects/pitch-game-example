$(document).ready(function() {

    displayCourt()
    openGraphAllData()

    $("#select_throw").change(function(e) {
        displayCourt();
    });
    
    $("#throwtype_buttons input").change(function(){
        displayCourt()
    })

    $("#select_from_session").on('change', () => {
        displayCourt()
    });
    $("#select_to_session").on('change', () => {
        displayCourt()
    });

    document.getElementById("defaultOpen").click();
})

function resetCourt() {
    document.getElementById("strikezone").src = '/strikezone.svg';
}

function displayCourt(){
    strikeZoneLoading()
    var throwtype = $('input[name=throwtype]:checked', '#throwtype_buttons').val()
    
    var id = getParameterByName("player_ID")

    var to_date 
    var from_date 
    
    //valid to date
    if(isValidDate(new Date($("#select_to_session").val()))){
        to_date = new Date($("#select_to_session").val())
    }
    else{
        to_date = new Date(Date.now())
        to_date = to_date.addDays(1)
    }
    //valid from date
    if(isValidDate(new Date($("#select_from_session").val()))){
        from_date = new Date($("#select_from_session").val())
    }
    else{
        from_date = new Date(1995, 11, 17)
    }

    var img = document.getElementById("strikeZoneImage")
    img.onload = function() { strikeZoneLoaded() }
    img.src = '/strikezone_analyze?throwtype='+throwtype+'&from_date='+from_date+'&to_date='+to_date+'&id='+id;

}

function openGraphAllData(){
    graphLoading()
    //build filter for all data
    var from_date = new Date(1995, 11, 17)
    var to_date = new Date(Date.now())
    var to_date = to_date.addDays(1)
    var clickLocation= "abcdefghijklm"
    var throwType =""

    var id = getParameterByName("player_ID")

    filter = {
        from_date : from_date,
        to_date: to_date,
        clickLocation: clickLocation,
        throwType: throwType
    }
    var filter = JSON.stringify(filter)
    //load image
    var img = document.getElementById("graph-image")
    img.onload = function() { graphLoaded() }
    img.src = '/graph/linegraph.png?filter='+filter+'&id='+id
}

async function OpenGraph(clickLocation){
    //show loading icon
    graphLoading()

    var dataObject = {'list_of_throws':[]}
    var to_date 
    var from_date 
    var throwType = $('input[name=throwtype]:checked', '#throwtype_buttons').val()
    var id = getParameterByName("player_ID")
    
    //valid to date
    if(isValidDate(new Date($("#select_to_session").val()))){
        to_date = new Date($("#select_to_session").val())
    }
    else{
        to_date = new Date(Date.now())
        to_date = to_date.addDays(1)
    }
    //valid from date
    if(isValidDate(new Date($("#select_from_session").val()))){
        from_date = new Date($("#select_from_session").val())
    }
    else{
        from_date = new Date(1995, 11, 17)
    }

    //Load graph for all locations
    if (clickLocation === "abcdefghijklm"){
        var img = document.getElementById("graph-image")
        img.onload = function() { graphLoaded() }
        filter = {
            from_date : from_date,
            to_date: to_date,
            clickLocation: clickLocation,
            throwType: throwType
        }
        filter = JSON.stringify(filter)
        img.src = '/graph/linegraph.png?filter='+filter+'&id='+id
    }
    //load graph for specific location
    else{
        //do some input capturing (click location to x/y coordinates)
        var PosX = 0;
        var PosY = 0;
        var ImgPos;
        var image = document.getElementById("strikeZoneImage");
        ImgPos = FindPosition(image);
        if (!e) var e = window.event;
        if (e.pageX || e.pageY) {
            PosX = e.pageX;
            PosY = e.pageY;
        } else if (e.clientX || e.clientY) {
            PosX = e.clientX + document.body.scrollLeft +
                document.documentElement.scrollLeft;
            PosY = e.clientY + document.body.scrollTop +
                document.documentElement.scrollTop;
        }
        PosX = ((PosX - ImgPos[0]) / image.clientWidth).toFixed(10);
        PosY = ((PosY - ImgPos[1]) / image.clientHeight).toFixed(10);
        
        dataObject['list_of_throws'].push({x: PosX, y: PosY}); 
        var dataString = JSON.stringify(dataObject);
        var msgJSON
        await $.ajax({
            type: "GET",
            url: "/coordinatesToAreas",
            data: {
                "hitdata": dataString, 
            },
            success: function (msg) {
                msgJSON = JSON.parse(msg)
                console.log("Working" + msg);
                clickLocation = Object.keys(msgJSON)[0]

                //moves some elements around
                var img = document.getElementById("graph-image")
                img.onload = function() { graphLoaded() }
                filter = {
                    from_date : from_date,
                    to_date: to_date,
                    clickLocation: clickLocation,
                    throwType: throwType
                }
                filter = JSON.stringify(filter)
                img.src = '/graph/linegraph.png?filter='+filter+'&id='+id

            },
            error: function (msg) {
                console.log('not working ' + JSON.stringify(msg));
            }
        });
    }
}


function FindPosition(oElement) {
    if (typeof (oElement.offsetParent) != "undefined") {
        for (var posX = 0, posY = 0; oElement; oElement = oElement.offsetParent) {
            posX += oElement.offsetLeft;
            posY += oElement.offsetTop;
        }
        return [posX, posY];
    } else {
        return [oElement.x, oElement.y];
    }
}

//loading functions (turns on and off loading icon)
function strikeZoneLoading(){
    document.getElementById("strikeZoneImage").style.display ="none";
    document.getElementById("loading-icon-strikezone").style.display = ""
}

function strikeZoneLoaded(){
    document.getElementById("strikeZoneImage").style.display="";
    document.getElementById("loading-icon-strikezone").style.display = "none"
    OpenGraph("abcdefghijklm")
}

function graphLoading(){
    document.getElementById("graph-image").style.display = "none"
    document.getElementById("loading-icon-graph").style.display = ""
}

function graphLoaded(){
    document.getElementById("graph-image").style.display = ""
    document.getElementById("loading-icon-graph").style.display = "none"
}


//Date helping functions
function isValidDate(d) {
    return d instanceof Date && !isNaN(d);
}

Date.prototype.addDays = function(days) {
    var date = new Date(this.valueOf());
    date.setDate(date.getDate() + days);
    return date;
}

//param helping function
function getParameterByName(name, url = window.location.href) {
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}