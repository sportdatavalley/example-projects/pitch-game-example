from .conn import conn
from .config import config

def login_db(emailAddress, password):
   db = conn()
   mycursor=db.cursor() 
   try:   
      mycursor.execute(f"SELECT * from User WHERE emailAddress = '{emailAddress}' AND passwordHash = '{password}'") 
      account = mycursor.fetchone()
      db.close()
      return account
   except:    
      db.rollback()  
   db.close()

def register_db(teamname, firstName, lastName, emailAddress, passwordHash):
   db=conn()
   mycursor=db.cursor()   
   try:   
      mycursor.execute(f"INSERT INTO User (teamname, firstName, lastName, passwordHash, emailAddress) VALUES ('{teamname}','{firstName}','{lastName}','{passwordHash}','{emailAddress}')")
      db.commit()
      db.close()
      return 'Successfully registered!'
      #if mycursor.rowcount == 0:
      #   mycursor.execute(f"INSERT INTO User (teamname, firstName, lastName, passwordHash, emailAddress) VALUES ('1','2','3','4','5')") 
      #   return 'done'
      #   db.commit()
      #   print('Successfully Registered!')   
      #else: 
      #   return "email address is already in use"
   except:    
      db.rollback()  
   db.close()